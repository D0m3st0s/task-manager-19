package ru.shumov.tm.service;

import ru.shumov.tm.endpoint.project.Project;
import ru.shumov.tm.endpoint.task.Task;
import ru.shumov.tm.endpoint.user.User;

public interface ToStringService {
    String projectToString(Project project);

    String taskToString(Task task);

    String userToString(User user);
}
