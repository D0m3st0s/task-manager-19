package ru.shumov.tm.service;

import ru.shumov.tm.Bootstrap;
import ru.shumov.tm.command.*;

import java.security.NoSuchAlgorithmException;
import java.util.*;

public class CommandsServiceImpl implements CommandsService {
    private Bootstrap bootstrap;
    private Map<String, AbstractCommand> commands = new LinkedHashMap<>();

    public CommandsServiceImpl(Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
    }

    public Collection<AbstractCommand> getList() {
        return commands.values();
    }

    public boolean checkKey(final String key) {
        return commands.containsKey(key);
    }

    public void commandExecute(final String key) throws NoSuchAlgorithmException {
        commands.get(key).execute();
    }

    public void commandAdd(AbstractCommand command) {
        command.setBootstrap(bootstrap);
        commands.put(command.getName(), command);
    }
}
