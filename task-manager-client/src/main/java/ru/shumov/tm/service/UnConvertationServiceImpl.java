package ru.shumov.tm.service;

//public class UnConvertationServiceImpl implements UnConvertationService{
//    private final Bootstrap bootstrap;
//
//    public UnConvertationServiceImpl(Bootstrap bootstrap) {
//        this.bootstrap = bootstrap;
//    }
//
//    public void load(String method) {
//        switch (method) {
//            case "ser java":
//                unSerialization();
//                break;
//            case "jaxb xml":
//                jaxbXmlUnConvertation();
//                break;
//            case "jaxb json":
//                jaxbJsonUnConvertation();
//                break;
//            case "fas xml":
//                fasterXmlUnConvertation();
//                break;
//            case "fas json":
//                fasterJsonUnConvertation();
//                break;
//        }
//    }
//
//    public void unSerialization() {
//        try {
//            var fileInputStreamProjects = new FileInputStream("projects.txt");
//            var objectInputStreamProjects = new ObjectInputStream(fileInputStreamProjects);
//
//            var fileInputStreamTasks = new FileInputStream("tasks.txt");
//            var objectInputStreamTasks = new ObjectInputStream(fileInputStreamTasks);
//
//            var projectDTO = (ProjectDTO) objectInputStreamProjects.readObject();
//            List<Project> projects = projectDTO.getProjects();
//            for (Project project : projects) {
//                bootstrap.getProjectService().update(project);
//            }
//            fileInputStreamProjects.close();
//            objectInputStreamProjects.close();
//
//            var taskDTO = (TaskDTO) objectInputStreamTasks.readObject();
//            List<Task> tasks = taskDTO.getTasks();
//            for (Task task : tasks) {
//                bootstrap.getTaskService().update(task);
//            }
//            fileInputStreamTasks.close();
//            objectInputStreamTasks.close();
//        }
//        catch (EOFException eofException) {
//
//        }
//        catch (Exception e) {
//            e.printStackTrace();
//        }
//    }
//
//    public void jaxbXmlUnConvertation() {
//        try {
//            JAXBContext contextProjects = JAXBContext.newInstance(ProjectDTO.class);
//            var unMarshallerProjects = contextProjects.createUnmarshaller();
//
//            var contextTasks = JAXBContext.newInstance(TaskDTO.class);
//            var unMarshallerTasks = contextTasks.createUnmarshaller();
//
//            var projectDTO = (ProjectDTO) unMarshallerProjects.unmarshal(new File("JAXBXmlProjects.xml"));
//            var projects = projectDTO.getProjects();
//            for (Project project : projects) {
//                bootstrap.getProjectService().update(project);
//            }
//
//            var taskDTO = (TaskDTO) unMarshallerTasks.unmarshal(new File("JAXBXmlTasks.xml"));
//            var tasks = taskDTO.getTasks();
//            for (Task task : tasks) {
//                bootstrap.getTaskService().update(task);
//            }
//        }
//        catch (Exception e) {
//            e.printStackTrace();
//        }
//    }
//
//    public void fasterJsonUnConvertation() {
//        try {
//            var objectMapper = new ObjectMapper();
//            var AnnotationIntrospector = new JacksonXmlAnnotationIntrospector();
//            objectMapper.setAnnotationIntrospector(AnnotationIntrospector);
//
//            var projectDTO = objectMapper.readValue(new File("fasterJsonProjects.json"), ProjectDTO.class);
//            var projects = projectDTO.getProjects();
//            for (Project project : projects) {
//                bootstrap.getProjectService().update(project);
//            }
//
//            var taskDTO = objectMapper.readValue(new File("fasterJsonTasks.json"), TaskDTO.class);
//            var tasks = taskDTO.getTasks();
//            for (Task task : tasks) {
//                bootstrap.getTaskService().update(task);
//            }
//        }
//        catch (Exception e) {
//            e.printStackTrace();
//        }
//    }
//    public void jaxbJsonUnConvertation() {
//        try {
//            var jaxbContextProjectDTO =  JAXBContext.newInstance(ProjectDTO.class);
//            var unMarshallerProjectDTO = jaxbContextProjectDTO.createUnmarshaller();
//            unMarshallerProjectDTO.setProperty(MarshallerProperties.MEDIA_TYPE, "application/json");
//            var jaxbContextTaskDTO = JAXBContext.newInstance(TaskDTO.class);
//            var unMarshallerTaskDTO = jaxbContextTaskDTO.createUnmarshaller();
//            unMarshallerTaskDTO.setProperty(MarshallerProperties.MEDIA_TYPE, "application/json");
//
//            var projectDTO = (ProjectDTO) unMarshallerProjectDTO.unmarshal(new File("JAXBJsonProjects.json"));
//            var projects = projectDTO.getProjects();
//            for (Project project : projects) {
//                bootstrap.getProjectService().update(project);
//            }
//
//            var taskDTO = (TaskDTO) unMarshallerTaskDTO.unmarshal(new File("JAXBJsonTasks.json"));
//            var tasks = taskDTO.getTasks();
//            for (Task task : tasks) {
//                bootstrap.getTaskService().update(task);
//            }
//        }
//        catch (Exception e) {
//            e.printStackTrace();
//        }
//    }
//    public void fasterXmlUnConvertation() {
//        try {
//            var xmlMapper = new XmlMapper();
//
//            var projectDTO = xmlMapper.readValue(new File("fasterXmlProjects.xml"), ProjectDTO.class);
//            var projects = projectDTO.getProjects();
//            for (Project project : projects) {
//                bootstrap.getProjectService().update(project);
//            }
//
//            var taskDTO = xmlMapper.readValue(new File("fasterXmlTasks.xml"), TaskDTO.class);
//            var tasks = taskDTO.getTasks();
//            for (Task task : tasks) {
//                bootstrap.getTaskService().update(task);
//            }
//        }
//        catch (Exception e) {
//            e.printStackTrace();
//        }
//    }
//}
