package ru.shumov.tm.service;

import ru.shumov.tm.endpoint.Session;

public interface ConvertationService {

    void save(String method, Session session);
}
