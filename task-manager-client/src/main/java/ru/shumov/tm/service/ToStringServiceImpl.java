package ru.shumov.tm.service;

import ru.shumov.tm.endpoint.project.Project;
import ru.shumov.tm.endpoint.task.Task;
import ru.shumov.tm.endpoint.user.User;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

public class ToStringServiceImpl implements ToStringService{
    private final DateFormat simpleDateFormat = new SimpleDateFormat("dd.MM.yyyy");

    public String projectToString(Project project) {
        return "Project{" +
                "id='" + project.getId() + '\'' +
                ", name='" + project.getName() + '\'' +
                ", description='" + project.getDescription() + '\'' +
                ", startDate=" + project.getStartDate() +
                ", endDate=" + project.getEndDate() +
                ", status=" + project.getStatus() +
                '}';
    }

    public String taskToString(Task task) {
        return "Task{" +
                "id='" + task.getId() + '\'' +
                ", name='" + task.getName() + '\'' +
                ", description='" + task.getDescription() + '\'' +
                ", startDate=" + task.getStartDate() +
                ", endDate=" + task.getEndDate() +
                ", projectId='" + task.getProjectId() + '\'' +
                ", status='" + task.getStatus() + '\'' +
                '}';
    }

    public String userToString(User user) {
        return "User{" +
                "login='" + user.getLogin() + '\'' +
                ", password='" + user.getPassword() + '\'' +
                ", role='" + user.getRole() + '\'' + '}';
    }
}
