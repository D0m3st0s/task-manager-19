package ru.shumov.tm.service;


import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.JacksonXmlAnnotationIntrospector;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import jakarta.xml.bind.JAXBContext;
import jakarta.xml.bind.Marshaller;
import org.eclipse.persistence.jaxb.MarshallerProperties;
import ru.shumov.tm.Bootstrap;
import ru.shumov.tm.Constants;
import ru.shumov.tm.dto.ProjectDTO;
import ru.shumov.tm.dto.TaskDTO;
import ru.shumov.tm.endpoint.Session;
import ru.shumov.tm.endpoint.project.Project;
import ru.shumov.tm.endpoint.task.Task;

import java.io.File;
import java.io.FileOutputStream;
import java.io.ObjectOutputStream;
import java.util.Collection;
import java.util.List;

import static jakarta.xml.bind.JAXBContext.newInstance;


public class ConvertationServiceImpl implements ConvertationService {
    private final Bootstrap bootstrap;

    public ConvertationServiceImpl(Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
    }

    public void save(String method, Session session) {
        switch (method) {
            case "ser java":
                serialization(session);
                break;
            case "jaxb xml":
                jaxbXmlConvertation(session);
                break;
            case "jaxb json":
                jaxbJsonConvertation(session);
                break;
            case "fas xml":
                fasterXmlConvertation(session);
                break;
            case "fas json":
                fasterJsonConvertation(session);
                break;
        }
    }

    public void serialization(Session session) {
        var projectDTO = new ProjectDTO();
        var taskDTO = new TaskDTO();
        try {
            var fileOutputStreamProjects = new FileOutputStream("projects.txt");
            var objectOutputStreamProjects = new ObjectOutputStream(fileOutputStreamProjects);

            var fileOutputStreamTasks = new FileOutputStream("tasks.txt");
            var objectOutputStreamTasks = new ObjectOutputStream(fileOutputStreamTasks);

            List<Project> projects = bootstrap.getProjectEndPoint().getList(session);
            projectDTO.getProjects().addAll(projects);
            objectOutputStreamProjects.writeObject(projectDTO);
            objectOutputStreamProjects.close();
            fileOutputStreamProjects.close();

            List<Task> tasks = bootstrap.getTaskEndPoint().getList(session);
            taskDTO.getTasks().addAll(tasks);
            objectOutputStreamTasks.writeObject(taskDTO);
            fileOutputStreamTasks.close();
            objectOutputStreamTasks.close();
            bootstrap.getTerminalService().outPutString(Constants.DONE);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public void jaxbXmlConvertation(Session session) {
        try {
            var projectDTO = new ProjectDTO();
            var taskDTO = new TaskDTO();
            JAXBContext contextProjects = newInstance(ProjectDTO.class);
            var marshallerProjects = contextProjects.createMarshaller();
            marshallerProjects.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);

            var contextTasks = newInstance(TaskDTO.class);
            var marshallerTasks = contextTasks.createMarshaller();
            marshallerTasks.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);

            Collection<Project> projects = bootstrap.getProjectEndPoint().getList(session);
            projectDTO.getProjects().addAll(projects);
            marshallerProjects.marshal(projectDTO, new File("JAXBXmlProjects.xml"));

            Collection<Task> tasks = bootstrap.getTaskEndPoint().getList(session);
            taskDTO.getTasks().addAll(tasks);
            marshallerTasks.marshal(taskDTO, new File("JAXBXmlTasks.xml"));
            bootstrap.getTerminalService().outPutString(Constants.DONE);
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }
    public void jaxbJsonConvertation(Session session) {
        try {
            var projectDTO = new ProjectDTO();
            var taskDTO = new TaskDTO();
            var jaxbContextProjectDTO =  newInstance(ProjectDTO.class);
            var marshallerProjectDTO = jaxbContextProjectDTO.createMarshaller();
            marshallerProjectDTO.setProperty(MarshallerProperties.MEDIA_TYPE, "application/json");
            var jaxbContextTaskDTO =  newInstance(TaskDTO.class);
            var marshallerTaskDTO = jaxbContextTaskDTO.createMarshaller();
            marshallerTaskDTO.setProperty(MarshallerProperties.MEDIA_TYPE, "application/json");

            Collection<Project> projects = bootstrap.getProjectEndPoint().getList(session);
            projectDTO.getProjects().addAll(projects);
            marshallerProjectDTO.marshal(projectDTO, new File("JAXBJsonProjects.json"));

            Collection<Task> tasks = bootstrap.getTaskEndPoint().getList(session);
            taskDTO.getTasks().addAll(tasks);
            marshallerTaskDTO.marshal(taskDTO, new File("JAXBJsonTasks.json"));
            bootstrap.getTerminalService().outPutString(Constants.DONE);
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }
    public void fasterJsonConvertation(Session session) {
        try {
            var projectDTO = new ProjectDTO();
            var taskDTO = new TaskDTO();
            var objectMapper = new ObjectMapper();
            var AnnotationIntrospector = new JacksonXmlAnnotationIntrospector();
            objectMapper.setAnnotationIntrospector(AnnotationIntrospector);

            Collection<Project> projects = bootstrap.getProjectEndPoint().getList(session);
            projectDTO.getProjects().addAll(projects);
            objectMapper.writeValue(new File("fasterJsonProjects.json"), projectDTO);

            Collection<Task> tasks = bootstrap.getTaskEndPoint().getList(session);
            taskDTO.getTasks().addAll(tasks);
            objectMapper.writeValue(new File("fasterJsonTasks.json"), taskDTO);
            bootstrap.getTerminalService().outPutString(Constants.DONE);
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }
    public void fasterXmlConvertation(Session session) {
        try {
            var projectDTO = new ProjectDTO();
            var taskDTO = new TaskDTO();
            var xmlMapper = new XmlMapper();

            Collection<Project> projects = bootstrap.getProjectEndPoint().getList(session);;
            projectDTO.getProjects().addAll(projects);
            xmlMapper.writeValue(new File("fasterXmlProjects.xml"), projectDTO);

            Collection<Task> tasks = bootstrap.getTaskEndPoint().getList(session);;
            taskDTO.getTasks().addAll(tasks);
            xmlMapper.writeValue(new File("fasterXmlTasks.xml"), taskDTO);
            bootstrap.getTerminalService().outPutString(Constants.DONE);
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }
}
