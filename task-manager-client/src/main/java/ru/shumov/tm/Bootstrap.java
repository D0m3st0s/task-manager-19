package ru.shumov.tm;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.reflections.Reflections;
import ru.shumov.tm.bootstrap.ServiceLocator;
import ru.shumov.tm.command.AbstractCommand;
import ru.shumov.tm.endpoint.Session;
import ru.shumov.tm.endpoint.project.ProjectEndPoint;
import ru.shumov.tm.endpoint.project.ProjectEndPointService;
import ru.shumov.tm.endpoint.session.SessionEndPoint;
import ru.shumov.tm.endpoint.session.SessionEndPointService;
import ru.shumov.tm.endpoint.task.TaskEndPoint;
import ru.shumov.tm.endpoint.task.TaskEndPointService;
import ru.shumov.tm.endpoint.user.User;
import ru.shumov.tm.endpoint.user.UserEndPoint;
import ru.shumov.tm.endpoint.user.UserEndPointService;
import ru.shumov.tm.exceptions.IncorrectInputException;
import ru.shumov.tm.service.*;
import ru.shumov.tm.service.TerminalServiceImpl;

import java.lang.Exception;
import java.util.Set;

import static ru.shumov.tm.Constants.*;

public class Bootstrap implements ServiceLocator {

    @Getter
    private TerminalServiceImpl terminalService = new TerminalServiceImpl();
    @Getter
    private CommandsService commandsService = new CommandsServiceImpl(this);
    @Getter
    private ToStringServiceImpl toStringService = new ToStringServiceImpl();
    @Getter
    private ProjectEndPoint projectEndPoint = new ProjectEndPointService().getProjectEndPointPort();
    @Getter
    private TaskEndPoint taskEndPoint = new TaskEndPointService().getTaskEndPointPort();
    @Getter
    private UserEndPoint userEndPoint = new UserEndPointService().getUserEndPointPort();
    @Getter
    private SessionEndPoint sessionEndPoint = new SessionEndPointService().getSessionEndPointPort();
    @Getter
    private ConvertationService convertationService = new ConvertationServiceImpl(this);
    @Getter
    private Md5Service md5Service = new Md5ServiceImpl();
    @Getter
    private boolean work = true;
    @Setter
    @Getter
    private User user;
    @Setter
    @Getter
    private Session session;
    public void setWork(boolean work) {
        this.work = work;
    }

    public void init() {
        try {
            initCommand();
            start();
        } catch (Exception exception) {
            terminalService.outPutString("При запуске приложения возникла ошибка.");
            exception.printStackTrace();
        }
    }

    public void start() {
        terminalService.outPutString(WELCOME);
        terminalService.outPutString(PROJECT_ID);
        while (work) {
            try {
                String commandName = terminalService.scanner();
                if (commandsService.checkKey(commandName)) {
                    commandsService.commandExecute(commandName);
                } else {
                    terminalService.outPutString(COMMAND_DOES_NOT_EXIST);
                }
            } catch (Exception exception) {
                terminalService.outPutString("При работе приложения возникла ошибка.");
                exception.printStackTrace();
            }
        }
    }

    public void initCommand() {
        AbstractCommand command;

        final Set<Class<? extends AbstractCommand>> classes =
                new Reflections("ru.shumov.tm.command").getSubTypesOf(AbstractCommand.class);
        try {
            for (@NotNull final Class clazz : classes) {
                if (AbstractCommand.class.isAssignableFrom(clazz)) {
                    command = (AbstractCommand) clazz.newInstance();
                    regCommand(command);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void regCommand(AbstractCommand command) {
        @NotNull final var commandName = command.getName();
        @NotNull final var commandDescription = command.getDescription();
        try {
            if (commandName == null || commandName.isEmpty()) {
                throw new IncorrectInputException("Invalid Command Name: " + commandName);
            }
            if (commandDescription == null || commandDescription.isEmpty()) {
                throw new IncorrectInputException("Invalid Command Description" + commandDescription);
            }
            commandsService.commandAdd(command);
        } catch (IncorrectInputException e) {
            terminalService.outPutString(e.getMassage());
        }
    }
}
