package ru.shumov.tm.bootstrap;

import ru.shumov.tm.endpoint.Session;
import ru.shumov.tm.endpoint.project.ProjectEndPoint;
import ru.shumov.tm.endpoint.session.SessionEndPoint;
import ru.shumov.tm.endpoint.task.TaskEndPoint;
import ru.shumov.tm.endpoint.user.User;
import ru.shumov.tm.endpoint.user.UserEndPoint;
import ru.shumov.tm.service.*;

public interface ServiceLocator {
    ProjectEndPoint getProjectEndPoint();
    TaskEndPoint getTaskEndPoint();
    UserEndPoint getUserEndPoint();
    SessionEndPoint getSessionEndPoint();
    TerminalServiceImpl getTerminalService();
    CommandsService getCommandsService();
    ToStringServiceImpl getToStringService();
    ConvertationService getConvertationService();
    void setWork(boolean work);
    User getUser();
    void setUser(User user);
    Session getSession();
    void setSession(Session session);
    Md5Service getMd5Service();
    void init();
}
