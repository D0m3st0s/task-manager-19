package ru.shumov.tm.dto;


import jakarta.xml.bind.annotation.XmlRootElement;
import lombok.Getter;
import lombok.Setter;
import ru.shumov.tm.endpoint.user.User;
;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@XmlRootElement
public class UserDTO implements Serializable {
    private List<User> users = new ArrayList<>();
}
