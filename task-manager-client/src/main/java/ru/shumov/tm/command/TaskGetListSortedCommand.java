package ru.shumov.tm.command;

import java.security.NoSuchAlgorithmException;
import java.util.Collection;

import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.Nullable;
import ru.shumov.tm.Constants;
import ru.shumov.tm.endpoint.task.Task;
import ru.shumov.tm.endpoint.user.Role;
public class TaskGetListSortedCommand extends AbstractCommand {
    private final Role role = Role.USER;
    @Getter
    private final String name = "task list sorted";
    @Getter
    private final String description = "task list sorted: Вывод всех задач с сортировкой.";

    @SneakyThrows
    @Override
    public void execute() throws NoSuchAlgorithmException {
        @Nullable final var user = bootstrap.getUser();
        @Nullable final var session = bootstrap.getSession();
        if (user == null) {
            bootstrap.getTerminalService().outPutString(Constants.NO_ROOTS);
            return;
        }
        if (user.getRole().equals(role)) {
            bootstrap.getTerminalService().outPutString("Как вы хотите отсортировать проекты:");
            bootstrap.getTerminalService().outPutString("by creating date/by start date/by end date/by status");
            var method = bootstrap.getTerminalService().scanner();
            Collection<Task> tasks = bootstrap.getTaskEndPoint().getSortedList(session, method);
            for (Task task : tasks) {
                bootstrap.getTerminalService()
                        .outPutString(bootstrap.getToStringService().taskToString(task));
            }
        }
    }
}
