package ru.shumov.tm.command;

import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shumov.tm.Constants;
import ru.shumov.tm.endpoint.project.Project;
import ru.shumov.tm.endpoint.task.Task;
import ru.shumov.tm.endpoint.user.Role;

import java.util.List;

public class ProjectRemoveCommand extends AbstractCommand {
    private final Role role = Role.USER;
    @Getter
    private final String name = "project remove";
    @Getter
    private final String description = "project remove: Выборочное удаление проектов.";

    @SneakyThrows
    @Override
    public void execute() {
        @Nullable final var user = bootstrap.getUser();
        @Nullable final var session = bootstrap.getSession();
        if (user == null) {
            bootstrap.getTerminalService().outPutString(Constants.NO_ROOTS);
            return;
        }
        if (user.getRole().equals(role)) {
            bootstrap.getTerminalService().outPutString(Constants.ENTER_PROJECT_ID_FOR_REMOVING);
            @NotNull var projectId = bootstrap.getTerminalService().scanner();
            final Project project = bootstrap.getProjectEndPoint().getOne(session, projectId);
            if (project == null) {
                bootstrap.getTerminalService().outPutString(Constants.PROJECT_DOES_NOT_EXIST);
                return;
            }
            if (project.getUserId().equals(user.getId())) {
                bootstrap.getProjectEndPoint().remove(session, projectId);
            } else {
                bootstrap.getTerminalService().outPutString(Constants.NO_ROOTS);
                return;
            }
            List<Task> values = bootstrap.getTaskEndPoint().getList(session);
            for (Task task : values) {
                if (task.getProjectId().equals(projectId) && task.getUserId().equals(user.getId())) {
                    bootstrap.getTaskEndPoint().remove(session, task.getId());
                }
            }
            bootstrap.getTerminalService().outPutString(Constants.DONE);
        } else {
            bootstrap.getTerminalService().outPutString(Constants.NO_ROOTS);
        }
    }

    public ProjectRemoveCommand() {
    }
}
