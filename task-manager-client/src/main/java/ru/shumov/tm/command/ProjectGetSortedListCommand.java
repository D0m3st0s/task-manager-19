package ru.shumov.tm.command;

import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.Nullable;
import ru.shumov.tm.Constants;
import ru.shumov.tm.endpoint.project.Project;
import ru.shumov.tm.endpoint.user.Role;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class ProjectGetSortedListCommand extends AbstractCommand {

    private final Role role = Role.USER;
    @Getter
    private final String name = "project list sorted";
    @Getter
    private final String description = "project list sorted: Вывод всех проектов c сортировкой.";

    @SneakyThrows
    @Override
    public void execute() throws NoSuchAlgorithmException {
        @Nullable final var user = bootstrap.getUser();
        @Nullable final var session = bootstrap.getSession();
        if (user == null) {
            bootstrap.getTerminalService().outPutString(Constants.NO_ROOTS);
            return;
        }
        if (user.getRole().equals(role)) {
            bootstrap.getTerminalService().outPutString("Как вы хотите отсортировать проекты:");
            bootstrap.getTerminalService().outPutString("by creating date/by start date/by end date/by status");
            var method = bootstrap.getTerminalService().scanner();
            List<Project> projects = bootstrap.getProjectEndPoint().getSortedList(session, method);
            for (Project project : projects) {
                bootstrap.getTerminalService()
                        .outPutString(bootstrap.getToStringService().projectToString(project));
            }
        }
    }
}
