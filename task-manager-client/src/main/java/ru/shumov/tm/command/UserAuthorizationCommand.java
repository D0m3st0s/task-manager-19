package ru.shumov.tm.command;

import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shumov.tm.Constants;

import java.security.NoSuchAlgorithmException;

public class UserAuthorizationCommand extends AbstractCommand {
    @Getter
    private final String name = "log in";
    @Getter
    private final String description = "log in: Авторизация пользователя.";

    @SneakyThrows
    public void execute() throws NoSuchAlgorithmException {
        @Nullable var user = bootstrap.getUser();
        if (user != null) {
            bootstrap.getTerminalService().outPutString(Constants.USER_ALREADY_AUTHORIZED);
            return;
        }
        bootstrap.getTerminalService().outPutString(Constants.ENTER_NAME_OF_USER);
        @NotNull var login = bootstrap.getTerminalService().scanner();
        try {user = bootstrap.getUserEndPoint().getOne(login);}
        catch (Exception e) {throw new RuntimeException(e);}
        if (user != null) {
            bootstrap.getTerminalService().outPutString(Constants.ENTER_PASSWORD);
            @NotNull final var password = bootstrap.getTerminalService().scanner();
            @NotNull final var output = bootstrap.getMd5Service().md5(password);
            if (output.equals(user.getPassword())) {
                bootstrap.setUser(user);
                bootstrap.setSession(bootstrap.getSessionEndPoint().openSession(user.getLogin()));
                bootstrap.getTerminalService().outPutString(Constants.AUTHORIZATION_SUCCESSFUL);
            } else {
                bootstrap.getTerminalService().outPutString(Constants.INVALID_PASSWORD);
            }
        } else {
            bootstrap.getTerminalService().outPutString(Constants.INVALID_LOGIN);
        }
    }

    public UserAuthorizationCommand() {
    }
}
