package ru.shumov.tm.command;

import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.Nullable;
import org.jetbrains.annotations.NotNull;
import ru.shumov.tm.Constants;
import ru.shumov.tm.endpoint.task.Task;
import ru.shumov.tm.endpoint.user.Role;

import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.UUID;

public class TaskCreateCommand extends AbstractCommand {
    private final Role role = Role.USER;
    @Getter
    private final String name = "task create";
    @Getter
    private final String description = "task create: Создание нового задания.";

    @SneakyThrows
    @Override
    public void execute() {
        @Nullable final var user = bootstrap.getUser();
        @Nullable final var session = bootstrap.getSession();
        if (user == null) {
            bootstrap.getTerminalService().outPutString(Constants.NO_ROOTS);
            return;
        }
        if (user.getRole().equals(role)) {
            DateFormat format = new SimpleDateFormat("dd.MM.yyyy");
            var date = new Date();
            GregorianCalendar calendar = new GregorianCalendar();
            calendar.setTime(date);
            XMLGregorianCalendar creatingDate = DatatypeFactory.newInstance().newXMLGregorianCalendar(calendar);
            bootstrap.getTerminalService().outPutString(Constants.ENTER_PROJECT_ID_FOR_TASKS);
            @NotNull final var projectId = bootstrap.getTerminalService().scanner();
            try {
                if (bootstrap.getProjectEndPoint().getOne(session, projectId) != null) {
                    bootstrap.getTerminalService().outPutString(Constants.ENTER_TASK_NAME);
                    @NotNull final var name = bootstrap.getTerminalService().scanner();
                    Task task = new Task();
                    task.setCreatingDate(creatingDate);
                    @NotNull final var userId = bootstrap.getUser().getId();
                    bootstrap.getTerminalService().outPutString(Constants.ENTER_START_DATE_OF_TASK);
                    @NotNull final var startDate = bootstrap.getTerminalService().scanner();
                    calendar.setTime(format.parse(startDate));
                    XMLGregorianCalendar XMLStartDate = DatatypeFactory.newInstance().newXMLGregorianCalendar(calendar);
                    task.setStartDate(XMLStartDate);
                    bootstrap.getTerminalService().outPutString(Constants.ENTER_DEADLINE_OF_TASK);
                    @NotNull final var endDate = bootstrap.getTerminalService().scanner();
                    calendar.setTime(format.parse(endDate));
                    XMLGregorianCalendar XMLEndDate = DatatypeFactory.newInstance().newXMLGregorianCalendar(calendar);
                    task.setEndDate(XMLEndDate);
                    @NotNull final var id = UUID.randomUUID().toString();
                    bootstrap.getTerminalService().outPutString(Constants.ENTER_DESCRIPTION_OF_TASK);
                    @NotNull final var description = bootstrap.getTerminalService().scanner();

                    task.setDescription(description);
                    task.setProjectId(projectId);
                    task.setName(name);
                    task.setId(id);
                    task.setUserId(userId);

                    bootstrap.getTaskEndPoint().create(session, task);
                    bootstrap.getTerminalService().outPutString(Constants.DONE);
                } else {
                    bootstrap.getTerminalService().outPutString(Constants.PROJECT_DOES_NOT_EXIST);
                }
            } catch (ParseException parseException) {
                bootstrap.getTerminalService().outPutString(Constants.INCORRECT_DATE_FORMAT);
            }
        } else {
            bootstrap.getTerminalService().outPutString(Constants.NO_ROOTS);
        }
    }

    public TaskCreateCommand() {
    }
}
