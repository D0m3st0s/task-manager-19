package ru.shumov.tm.command;

import com.jcabi.manifests.Manifests;
import lombok.Getter;


import java.security.NoSuchAlgorithmException;

@Getter
public class AboutCommand extends AbstractCommand {
    private final String name = "about";
    private final String description = "about: Информация о сборке приложения.";

    public void execute() throws NoSuchAlgorithmException {
        System.out.println("[ABOUT]");
        System.out.println("Version of build: " + Manifests.read("BuildNumber"));
        System.out.println("Built by: " + Manifests.read("Built-By"));

    }
}
