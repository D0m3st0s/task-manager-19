package ru.shumov.tm.command;

import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shumov.tm.Constants;
import ru.shumov.tm.endpoint.user.Role;

public class TaskRemoveCommand extends AbstractCommand {
    private final Role role = Role.USER;
    @Getter
    private final String name = "task remove";
    @Getter
    private final String description = "task remove: Выборочное удаление задач.";

    @SneakyThrows
    @Override
    public void execute() {
        @Nullable final var user = bootstrap.getUser();
        @Nullable final var session = bootstrap.getSession();
        if (user == null) {
            bootstrap.getTerminalService().outPutString(Constants.NO_ROOTS);
            return;
        }
        if (user.getRole().equals(role)) {
            bootstrap.getTerminalService().outPutString(Constants.ENTER_TASK_ID_FOR_REMOVING);
            @NotNull final var taskId = bootstrap.getTerminalService().scanner();
            final var task = bootstrap.getTaskEndPoint().getOne(session, taskId);
            if (task == null) {
                bootstrap.getTerminalService().outPutString(Constants.TASK_DOES_NOT_EXIST);
                return;
            }
            if (task.getUserId().equals(user.getId())) {
                bootstrap.getTaskEndPoint().remove(session, taskId);
            }
            bootstrap.getTerminalService().outPutString(Constants.DONE);
        } else {
            bootstrap.getTerminalService().outPutString(Constants.NO_ROOTS);
        }
    }

    public TaskRemoveCommand() {
    }
}
