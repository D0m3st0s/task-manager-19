package ru.shumov.tm.command;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

@Getter @Setter
public class SaveCommand extends AbstractCommand{
    private String name = "save";
    private String description = "save: Сохранение проектов и задач.";
    @Override
    public void execute() {
        @Nullable final var session = bootstrap.getSession();
        bootstrap.getTerminalService().outPutString("В каком формате вы хотите сохранить данные?");
        bootstrap.getTerminalService().outPutString("ser java || jaxb xml || jaxb json || fas xml || fas json");
        final var answer = bootstrap.getTerminalService().scanner();
        bootstrap.getConvertationService().save(answer, session);
    }
}
