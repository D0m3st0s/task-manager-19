package ru.shumov.tm.application;

import ru.shumov.tm.Bootstrap;

public class Application {
    public static void main(String[] args){
        Bootstrap bootstrap = new Bootstrap();
        bootstrap.init();
    }
}
