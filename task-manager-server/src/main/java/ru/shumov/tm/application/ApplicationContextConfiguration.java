package ru.shumov.tm.application;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.*;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import ru.shumov.tm.bootstrap.Bootstrap;
import ru.shumov.tm.service.*;


import javax.sql.DataSource;
import java.util.Properties;

@Configuration
@EnableTransactionManagement
@EnableJpaRepositories("ru.shumov.tm.repository")
public class ApplicationContextConfiguration {
    @Bean(name = "datasource")
    @Primary
    public DataSource dataSource() {
        final DriverManagerDataSource dataSource =
                new DriverManagerDataSource();
        dataSource.setDriverClassName("com.mysql.jdbc.Driver");
        dataSource.setUrl("jdbc:mysql://localhost:3306/Task Manager");
        dataSource.setUsername("root");
        dataSource.setPassword("root");
        return dataSource;
    }
    @Bean(name = "entityManagerFactory")
    @Primary
    public LocalContainerEntityManagerFactoryBean entityManagerFactory(@Qualifier(value = "datasource") final DataSource dataSource) {
        final LocalContainerEntityManagerFactoryBean factoryBean;
        factoryBean = new LocalContainerEntityManagerFactoryBean();
        factoryBean.setDataSource(dataSource);
        factoryBean.setJpaVendorAdapter(new HibernateJpaVendorAdapter());
        factoryBean.setPackagesToScan("ru.shumov.tm.repository");
        final Properties properties = new Properties();
        properties.put("hibernate.show_sql", "true");
        properties.put("hibernate.hbm2ddl.auto", "update");
        properties.put("hibernate.dialect",
                "org.hibernate.dialect.MySQL5Dialect");
        factoryBean.setJpaProperties(properties);
        return factoryBean;
    }
    @Bean(name = "transactionManager")
    @Primary
    public PlatformTransactionManager platformTransactionManager(
           @Qualifier("entityManagerFactory") final LocalContainerEntityManagerFactoryBean emf
    ) {
        final JpaTransactionManager platformTransactionManager =
                new JpaTransactionManager();
        platformTransactionManager.setEntityManagerFactory(emf.getObject());
        return platformTransactionManager;
    }
    @Bean
    @Scope("singleton")
    public Bootstrap bootstrap() {
        return new Bootstrap();
    }
    @Bean
    public ProjectService projectService() {
        return new ProjectServiceImpl();
    }
    @Bean
    @Scope("singleton")
    public SessionService sessionService() {
        return new SessionServiceImpl();
    }
    @Bean
    public UserService userService() {
        return new UserServiceImpl();
    }
    @Bean
    public TaskService taskService() {
        return new TaskServiceImpl();
    }
    @Bean
    public Md5Service md5Service() {
        return new Md5ServiceImpl();
    }
}
