package ru.shumov.tm.application;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import ru.shumov.tm.bootstrap.Bootstrap;

public class Application {
    public static void main(String[] args){
        ApplicationContext context = new AnnotationConfigApplicationContext(ApplicationContextConfiguration.class);
        context.getBean(Bootstrap.class).main();
    }
}
