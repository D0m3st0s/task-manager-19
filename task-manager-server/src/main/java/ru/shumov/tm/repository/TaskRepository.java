package ru.shumov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ru.shumov.tm.repository.entity.Task;


import java.util.List;
@Repository
public interface TaskRepository extends CrudRepository<Task, String> {
    @Modifying
    @Query(value = "select t from Task t where t.userId = :id")
    List<Task> findAllByUserId(@Param ("id")@NotNull String id);
    @Modifying
    @Query(value = "SELECT t FROM Task t WHERE t.id = :id AND t.userId = :userId")
    Task findOne(@Param(value = "id") String id, @Param(value = "userId") String userId);

}
