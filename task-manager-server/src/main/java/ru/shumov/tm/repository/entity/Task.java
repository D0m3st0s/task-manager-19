package ru.shumov.tm.repository.entity;


import lombok.Getter;
import lombok.Setter;
import ru.shumov.tm.enums.Status;

import jakarta.xml.bind.annotation.XmlRootElement;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Date;

@Getter
@Setter
@XmlRootElement
@Entity
@Table(name = "Tasks")
public class Task extends ru.shumov.tm.repository.entity.Entity implements Serializable {
    @Id
    private String id;
    private String name;
    private String description;
    @Column(name = "creating_date")
    private Date creatingDate;
    @Column(name = "start_date")
    private Date startDate;
    @Column(name = "end_date")
    private Date endDate;
    @Column(name = "project_id")
    private String projectId;
    @Column(name = "user_id")
    private String userId;
    private String status = Status.PLANNED.toString();
}
