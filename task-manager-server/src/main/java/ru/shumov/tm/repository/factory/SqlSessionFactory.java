package ru.shumov.tm.repository.factory;

import org.apache.ibatis.datasource.pooled.PooledDataSource;
import org.apache.ibatis.mapping.Environment;
import org.apache.ibatis.session.Configuration;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.apache.ibatis.transaction.TransactionFactory;
import org.apache.ibatis.transaction.jdbc.JdbcTransactionFactory;
import org.jetbrains.annotations.Nullable;
import ru.shumov.tm.repository.ProjectRepository;
import ru.shumov.tm.repository.SessionRepository;
import ru.shumov.tm.repository.TaskRepository;
import ru.shumov.tm.repository.UserRepository;

import javax.sql.DataSource;

public class SqlSessionFactory {
    public org.apache.ibatis.session.SqlSessionFactory getSqlSessionFactory() {
        @Nullable final String user = "root";
        @Nullable final String password = "root";
        @Nullable final String url = "jdbc:mysql://localhost/Task Manager?useUnicode=true&serverTimezone=UTC";
        @Nullable final String driver = "com.mysql.jdbc.Driver";
        final DataSource dataSource =
                new PooledDataSource(driver, url, user, password);
        final TransactionFactory transactionFactory =
                new JdbcTransactionFactory();
        final Environment environment =
                new Environment("development", transactionFactory, dataSource);
        final Configuration configuration = new Configuration(environment);
        configuration.addMapper(ProjectRepository.class);
        configuration.addMapper(SessionRepository.class);
        configuration.addMapper(TaskRepository.class);
        configuration.addMapper(UserRepository.class);
        return new SqlSessionFactoryBuilder().build(configuration);
    }
}
