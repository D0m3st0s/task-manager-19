package ru.shumov.tm.repository.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.UUID;

@NoArgsConstructor
@Getter
@Setter
@Entity
@Table(name = "Sessions")
public class Session extends ru.shumov.tm.repository.entity.Entity implements Cloneable {

    public Session clone() {
        try {
            return (Session) super.clone();
        }
        catch (CloneNotSupportedException e) {
            return null;
        }
    }
    @Id
    private String id = UUID.randomUUID().toString();
    private Long timestamp;
    @Column(name = "user_id")
    private String userId;
    private String signature;
    @Column(name = "owner_id")
    private String ownerId;
}
