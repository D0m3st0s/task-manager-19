package ru.shumov.tm.repository;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ru.shumov.tm.repository.entity.Project;

import java.util.List;
@Repository("projectRepository")
public interface ProjectRepository extends CrudRepository<Project, String> {
    @Query(value = "SELECT p FROM Project p WHERE p.id = :id AND p.userId = :userId")
    Project findOne(@Param(value = "id") String id, @Param(value = "userId") String userId);
    @Modifying
    @Query(value = "select p from Project p where p.userId = :id")
    List<Project> findAllByUserId(@Param("id") String id);
}
