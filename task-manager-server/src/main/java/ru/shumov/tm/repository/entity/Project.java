package ru.shumov.tm.repository.entity;

import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlRootElement;
import lombok.Getter;
import lombok.Setter;
import ru.shumov.tm.enums.Status;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Date;

@Getter
@Setter
@XmlRootElement
@XmlAccessorType
@javax.persistence.Entity
@Table(name = "Projects")
public class Project extends Entity implements Serializable {
    @Id
    private String id;
    private String name;
    private String description;
    @Column(name = "creating_date")
    private Date creatingDate;
    @Column(name = "start_date")
    private Date startDate;
    @Column(name = "end_date")
    private Date endDate;
    @Column(name = "user_id")
    private String userId;
    private String status = Status.PLANNED.toString();
}
