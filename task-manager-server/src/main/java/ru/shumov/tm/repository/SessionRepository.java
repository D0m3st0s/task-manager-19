package ru.shumov.tm.repository;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import ru.shumov.tm.repository.entity.Session;

import java.util.List;

public interface SessionRepository extends CrudRepository<Session, String> {
    @Modifying
    @Query(value = "select s from Session s")
    List<Session> findAll();
    @Modifying
    @Query(value = "SELECT s FROM Session s WHERE s.id = :id")
    Session findOne(@Param("id")@NotNull String id);
}
