package ru.shumov.tm.repository;

import org.apache.ibatis.annotations.*;
import ru.shumov.tm.repository.entity.User;

import java.util.List;

public interface UserRepository {
    @Insert("INSERT INTO users (id,login,password,role) VALUES (#{id}, #{login}, #{password}, #{role})")
    void persist(User user);
    @Update("UPDATE users SET login=#{login}, password=#{password}, role=#{role} WHERE id=#{id}")
    void merge(User user);
    @Select("SELECT * FROM users WHERE login=#{login}")
    @Results(id = "user", value = {
            @Result(column = "id",property = "id"),
            @Result(column = "login",property = "login"),
            @Result(column = "password",property = "password"),
            @Result(column = "role",property = "role")
    })
    User findOne(String login);
    @Select("SELECT * FROM users")
    @Results(id = "users", value = {
            @Result(column = "id",property = "id"),
            @Result(column = "login",property = "login"),
            @Result(column = "password",property = "password"),
            @Result(column = "role",property = "role")
    })
    List<User> findAll();
}
