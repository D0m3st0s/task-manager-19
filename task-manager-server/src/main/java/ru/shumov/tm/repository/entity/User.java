package ru.shumov.tm.repository.entity;
import jakarta.xml.bind.annotation.XmlRootElement;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.UUID;

@Getter
@Setter
@XmlRootElement
@Entity
@Table(name = "Users")
public class User extends ru.shumov.tm.repository.entity.Entity implements Serializable {
    private String id = UUID.randomUUID().toString();
    private String role;
    private String password;
    @Id
    private String login;

    @Override
    public String toString() {
        return "User{" +
                "role=" +
                ", login='" + login + '\'' + id +
                '}';
    }
}

