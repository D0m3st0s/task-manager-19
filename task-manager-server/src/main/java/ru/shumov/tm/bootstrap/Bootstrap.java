package ru.shumov.tm.bootstrap;

import jakarta.xml.ws.Endpoint;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import ru.shumov.tm.endpoint.*;
import ru.shumov.tm.service.*;

import java.sql.Connection;

@NoArgsConstructor
@Getter
@Setter
@Component
@Scope("singleton")
public class Bootstrap implements ServiceLocator {
    private Connection connection;
    @Bean
    public void main() {
        publishEndpoints();
        initConnection();
    }

    private void publishEndpoints(){
        Endpoint.publish("http://localhost:8080/ProjectEndpoint?wsdl",
                new ProjectEndPoint());
        Endpoint.publish("http://localhost:8080/TaskEndpoint?wsdl",
                new TaskEndPoint());
        Endpoint.publish("http://localhost:8080/UserEndpoint?wsdl",
                new UserEndPoint());
        Endpoint.publish("http://localhost:8080/SessionEndpoint?wsdl",
                new SessionEndPoint());

        System.out.println("http://localhost:8080/ProjectEndpoint?wsdl");
        System.out.println("http://localhost:8080/TaskEndpoint?wsdl");
        System.out.println("http://localhost:8080/UserEndpoint?wsdl");
        System.out.println("http://localhost:8080/SessionEndpoint?wsdl");
    }

    public void initConnection() {
        this.connection = Connector.connectionDB();
    }
}
