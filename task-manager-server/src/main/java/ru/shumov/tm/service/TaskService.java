package ru.shumov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shumov.tm.repository.entity.Task;

import java.util.List;

public interface TaskService {

    void create(Task task);

    void update(Task task);

    void clear();

    void remove(String taskId, String userId);

    List<Task> getList(@NotNull String id);

    List<Task> getSortedList(String id, String method);

    Task getOne(String id, @Nullable String userId);

    List<Task> find(String id, String part);
}