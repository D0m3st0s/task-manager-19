package ru.shumov.tm.service;

import org.hibernate.query.Query;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.shumov.tm.Constants;
import ru.shumov.tm.repository.TaskRepository;
import ru.shumov.tm.repository.entity.Task;
import ru.shumov.tm.exceptions.IncorrectInputException;
import ru.shumov.tm.repository.factory.EntityManagerFactory;

import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

@Service
public class TaskServiceImpl implements TaskService {
    @Autowired
    private TaskRepository taskRepository;
    @Bean
    @Transactional
    public void create(@NotNull Task task) {
        try {
            if (task.getName() == null || task.getName().isEmpty()) {
                throw new IncorrectInputException(Constants.EXCEPTION_TASK + "task name");
            }
            if (task.getProjectId() == null || task.getProjectId().isEmpty()) {
                throw new IncorrectInputException(Constants.EXCEPTION_TASK + "project id");
            }
            taskRepository.save(task);
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }
    @Bean
    @Transactional
    public void clear() {
        try {
            taskRepository.deleteAll();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    @Bean
    @Transactional
    public void remove(@NotNull String taskId, String userId) {
        try {
            taskRepository.delete(taskRepository.findOne(taskId, userId));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    @Bean
    public List<Task> getList(@NotNull String id) {
        return taskRepository.findAllByUserId(id);
    }
    @Bean
    public List<Task> getSortedList(String id, String method) {
        List<Task> tasks;
        tasks = taskRepository.findAllByUserId(id);
        sorting(method, tasks);
        return tasks;
    }
    @Bean
    public Task getOne(@NotNull String id, String userId) {
        Task task;
        try {
            task = taskRepository.findOne(id, userId);
            if(task.getUserId().equals(userId)) {
                return  task;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
    @Bean
    public List<Task> find(String id, String part) {
        List<Task> tasks = new ArrayList<>();
        try {
            List<Task> values = taskRepository.findAllByUserId(id);
            for (Task task : values) {
                if (task.getName().contains(part) || task.getDescription().contains(part)) {
                    tasks.add(task);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return tasks;
    }
    @Bean
    @Transactional
    public void update(@NotNull Task task) {
        try {
            if (task.getName() == null || task.getName().isEmpty()) {
                throw new IncorrectInputException(Constants.EXCEPTION_TASK + "task name");
            }
            if (task.getProjectId() == null || task.getProjectId().isEmpty()) {
                throw new IncorrectInputException(Constants.EXCEPTION_TASK + "project id");
            }
            taskRepository.save(task);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public void sorting(@NotNull final String method, @NotNull final List<Task> tasks) {
        switch (method) {
            case "by creating date":
                tasks.sort(Comparator.comparing(Task::getCreatingDate));
                break;
            case "by start date":
                tasks.sort(Comparator.comparing(Task::getStartDate));
                break;
            case "by end date":
                tasks.sort(Comparator.comparing(Task::getEndDate));
                break;
            case "by status":
                tasks.sort(Comparator.comparing(Task::getStatus));
                break;
        }

    }
}
