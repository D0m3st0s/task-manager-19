package ru.shumov.tm.service;

import lombok.Getter;
import lombok.NoArgsConstructor;

import lombok.Setter;
import org.hibernate.query.Query;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;
import ru.shumov.tm.application.ApplicationContextConfiguration;
import ru.shumov.tm.repository.SessionRepository;
import ru.shumov.tm.repository.entity.Session;
import ru.shumov.tm.repository.entity.User;
import ru.shumov.tm.exceptions.ValidateException;
import ru.shumov.tm.repository.factory.EntityManagerFactory;

import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;
import java.util.UUID;
@NoArgsConstructor
@Getter
@Setter
@Service
@Scope("singleton")
public class SessionServiceImpl implements SessionService{

    private final String id = UUID.randomUUID().toString();
    @Autowired
    private SessionRepository sessionRepository;

    public void validate(final Session session) throws ValidateException {
        if(session == null) throw new ValidateException();
        if(session.getSignature() == null || session.getSignature().isEmpty()) throw new ValidateException();
        if(session.getUserId() == null || session.getUserId().isEmpty()) throw new ValidateException();
        if(session.getTimestamp() == null) throw new ValidateException();
        final Session value = session.clone();
        final String sessionSignature = session.getSignature();
        final String valueSignature = sing(value).getSignature();
        final boolean check = sessionSignature.equals(valueSignature);
        try {
            if (!check) throw new ValidateException();
            Session sessionInDB = sessionRepository.findOne(session.getId());
            if (sessionInDB == null) throw new ValidateException();
        } catch (Exception ignored) {
        }
    }

    public Session sing(final Session session) {
        if(session == null) {return null;}
        session.setSignature(null);
        final String signature = SignatureService.sign(session, "Ништяк пацаны, сегодня кайфуем", 365);
        session.setSignature(signature);
        return session;
    }
    @Bean
    public Session openSession(final String login) {
        ApplicationContext context = new AnnotationConfigApplicationContext(ApplicationContextConfiguration.class);
        Session finalSession = null;
        try {
            final User user = context.getBean(ApplicationContextConfiguration.class).userService().getOne(login);
            final Session session = new Session();
            session.setUserId(user.getId());
            session.setOwnerId(id);
            session.setTimestamp(1000L);
            finalSession = sing(session);
            if (finalSession == null) throw new ValidateException();
            sessionRepository.save(session);
        } catch (BeansException | ValidateException e) {
            e.printStackTrace();
        }
        return finalSession;
    }
    @Bean
    public void closeSession(final Session session) {
        try {
            validate(session);
            sessionRepository.delete(session);
        } catch (ValidateException e) {
            e.printStackTrace();
        }
    }
    @Bean
    public List<Session> getSessionList() {
        List<Session> sessions;
        sessions = sessionRepository.findAll();
        return sessions;
    }
}
