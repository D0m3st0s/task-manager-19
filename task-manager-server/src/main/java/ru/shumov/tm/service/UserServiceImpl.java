package ru.shumov.tm.service;

import lombok.SneakyThrows;
import org.hibernate.query.Query;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.shumov.tm.application.ApplicationContextConfiguration;
import ru.shumov.tm.repository.entity.User;
import ru.shumov.tm.enums.Role;
import ru.shumov.tm.repository.factory.EntityManagerFactory;

import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.security.NoSuchAlgorithmException;
import java.util.List;
import java.util.UUID;
@Service
public class UserServiceImpl implements UserService {
    javax.persistence.EntityManagerFactory managerFactory =  new EntityManagerFactory().factory();
    EntityManager manager = managerFactory.createEntityManager();
    @Bean
    public List<User> getList() {
        javax.persistence.EntityManagerFactory managerFactory =  new EntityManagerFactory().factory();
        EntityManager manager = managerFactory.createEntityManager();
        try {
            CriteriaBuilder builder = manager.getCriteriaBuilder();
            CriteriaQuery<User> criteriaQuery = builder.createQuery(User.class);

            Root<User> root = criteriaQuery.from(User.class);
            criteriaQuery.select(root);

            Query<User> query = (Query<User>) manager.createQuery(criteriaQuery);
            return query.getResultList();
        } finally {
            manager.close();
        }
    }
    @Bean
    public User getOne(@NotNull String login) {
        javax.persistence.EntityManagerFactory managerFactory =  new EntityManagerFactory().factory();
        EntityManager manager = managerFactory.createEntityManager();
        try {
            return manager.find(User.class, login);
        } finally {
            manager.close();
        }
    }
    @Bean
    public void create(@NotNull String login, String userPassword) {
        javax.persistence.EntityManagerFactory managerFactory =  new EntityManagerFactory().factory();
        EntityManager manager = managerFactory.createEntityManager();
        ApplicationContext context = new AnnotationConfigApplicationContext(ApplicationContextConfiguration.class);
        User user = new User();
        user.setId(UUID.randomUUID().toString());
        user.setLogin(login);
        try {
            String password = context.getBean(Md5ServiceImpl.class).md5(userPassword);
            user.setPassword(password);
        }
        catch (NoSuchAlgorithmException e) {throw new RuntimeException(e);}
        user.setRole(Role.USER.toString());
        try {
            manager.getTransaction().begin();
            manager.persist(user);
            manager.getTransaction().commit();
        } finally {
            manager.close();
        }
    }
    @Bean
    @Transactional
    public void update(@NotNull User user) {
        javax.persistence.EntityManagerFactory managerFactory =  new EntityManagerFactory().factory();
        EntityManager manager = managerFactory.createEntityManager();
        try {
            manager.getTransaction().begin();
            manager.merge(user);
            manager.getTransaction().commit();
        } finally {
            manager.close();
        }
    }
    @Bean
    @SneakyThrows
    @Transactional
    public void passwordUpdate(User user, String password) {
        ApplicationContext context = new AnnotationConfigApplicationContext(ApplicationContextConfiguration.class);
        javax.persistence.EntityManagerFactory managerFactory =  new EntityManagerFactory().factory();
        EntityManager manager = managerFactory.createEntityManager();
        var md5Password = context.getBean(Md5ServiceImpl.class).md5(password);
        user.setPassword(md5Password);
        try {
            manager.getTransaction().begin();
            manager.merge(user);
            manager.getTransaction().commit();
        } finally {
            manager.close();
        }
    }
}
