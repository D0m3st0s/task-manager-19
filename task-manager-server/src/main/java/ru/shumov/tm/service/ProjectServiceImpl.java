package ru.shumov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.shumov.tm.Constants;
import ru.shumov.tm.repository.entity.Project;
import ru.shumov.tm.repository.entity.Task;
import ru.shumov.tm.exceptions.IncorrectInputException;
import ru.shumov.tm.repository.ProjectRepository;

import java.util.*;

@Service
public class ProjectServiceImpl implements ProjectService {

    @Autowired
    private ProjectRepository projectRepository;
    @Autowired
    private TaskService taskService;

    @Transactional
    public void create(@NotNull Project project) {
        try {
            if (project.getName() == null || project.getName().isEmpty()) {
                throw new IncorrectInputException(Constants.EXCEPTION_PROJECT + "project name");
            }
            projectRepository.save(project);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    @Transactional
    public void clear() {
        try {
            projectRepository.deleteAll();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    @Bean
    @Transactional
    public void remove(@Nullable String projectId, String userId) {
        try {
            if (projectId == null || projectId.isEmpty()) {
                throw new IncorrectInputException(Constants.EXCEPTION_PROJECT + "project id");
            }
            List<Task> tasks = taskService.getList(userId);
            for(Task task : tasks) {
                if(task.getProjectId().equals(projectId)) {
                    taskService.remove(task.getId(), userId);
                }
            }
            Project project = projectRepository.findOne(projectId, userId);
            projectRepository.delete(project);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    @Bean
    public List<Project> getList(String id) {
        return projectRepository.findAllByUserId(id);
    }
    @Bean
    public Project getOne(@NotNull String id, String userId) {
        Project project;
        try {
            project = projectRepository.findOne(id, userId);
            if(project.getUserId().equals(userId)) {
                return project;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
    @Bean
    @Transactional
    public void update(@NotNull Project project) {
        try {
            if (project.getName() == null || project.getName().isEmpty()) {
                throw new IncorrectInputException(Constants.EXCEPTION_PROJECT + "project name");
            }
            projectRepository.save(project);
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }
    @Bean
    public List<Project> getSortedList(String id, String method) {
        List<Project> values = null;
        try {
            values = projectRepository.findAllByUserId(id);
            sorting(method, values);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return values;
    }
    @Bean
    public List<Project> find(String id, String part){
        List<Project> projects = new ArrayList<>();
        try {
            List<Project> values = projectRepository.findAllByUserId(id);
            for (Project project : values) {
                if (project.getName().equals(part) || project.getDescription().equals(part)) {
                    projects.add(project);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return projects;
    }
    private void sorting(@NotNull final String method, @NotNull final List<Project> projects) {
        switch (method) {
            case "by creating date":
                projects.sort(Comparator.comparing(Project::getCreatingDate));
                break;
            case "by start date":
                projects.sort(Comparator.comparing(Project::getStartDate));
                break;
            case "by end date":
                projects.sort(Comparator.comparing(Project::getEndDate));
                break;
            case "by status":
                projects.sort(Comparator.comparing(Project::getStatus));
                break;
        }
    }
}
