package ru.shumov.tm.service;

import ru.shumov.tm.repository.entity.Project;

import java.util.List;

public interface ProjectService {

    void create(Project project);

    void update(Project project);

    void clear();

    void remove(String projectId, String userId);

    List<Project> getSortedList(String id, String method);

    List<Project> find(String id, String part);

    List<Project> getList(String id);

    Project getOne(String id, String userID);
}