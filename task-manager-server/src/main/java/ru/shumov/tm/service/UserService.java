package ru.shumov.tm.service;

import ru.shumov.tm.repository.entity.User;

import java.util.Collection;

public interface UserService {

    User getOne(String login);

    void create(String login, String password);

    void update(User user);

    void passwordUpdate(User user, String password);

    Collection<User> getList();
}
