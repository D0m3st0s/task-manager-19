package ru.shumov.tm.endpoint;

import jakarta.jws.WebParam;
import jakarta.jws.WebService;
import lombok.NoArgsConstructor;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Controller;
import ru.shumov.tm.application.ApplicationContextConfiguration;
import ru.shumov.tm.repository.entity.Session;
import ru.shumov.tm.exceptions.EndpointException;
import ru.shumov.tm.service.SessionService;

@NoArgsConstructor
@WebService
@Controller
public class SessionEndPoint {
    ApplicationContext context = new AnnotationConfigApplicationContext(ApplicationContextConfiguration.class);
    @Bean
    public Session openSession(
            @WebParam(name = "login") final String login) throws Exception {
        SessionService sessionService = context.getBean(ApplicationContextConfiguration.class).sessionService();
        if(sessionService == null) throw new EndpointException();
        return sessionService.openSession(login);
    }
    @Bean
    public void closeSession(
            @WebParam(name = "session") final Session session) throws Exception {
        SessionService sessionService = context.getBean(ApplicationContextConfiguration.class).sessionService();
        if(sessionService == null) throw new EndpointException();
        sessionService.validate(session);
        sessionService.closeSession(session);
    }
}
