package ru.shumov.tm.endpoint;

import lombok.NoArgsConstructor;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Controller;
import ru.shumov.tm.application.ApplicationContextConfiguration;
import ru.shumov.tm.repository.entity.Session;
import ru.shumov.tm.repository.entity.Task;
import ru.shumov.tm.exceptions.EndpointException;
import ru.shumov.tm.service.*;

import jakarta.jws.WebParam;
import jakarta.jws.WebService;

import java.util.List;

@NoArgsConstructor
@WebService
@Controller
public class TaskEndPoint {
    ApplicationContext context = new AnnotationConfigApplicationContext(ApplicationContextConfiguration.class);
    @Bean
    public void create(
            @WebParam(name = "session") final Session session,
            @WebParam(name = "task") final Task task) throws Exception {
        TaskService taskService = context.getBean(ApplicationContextConfiguration.class).taskService();
        SessionService sessionService = context.getBean(ApplicationContextConfiguration.class).sessionService();
        if(taskService == null || sessionService == null) throw new EndpointException();
        sessionService.validate(session);
        taskService.create(task);
    }
    @Bean
    public List<Task> getList(
            @WebParam(name = "session") final Session session) throws Exception {
        TaskService taskService = context.getBean(ApplicationContextConfiguration.class).taskService();
        SessionService sessionService = context.getBean(ApplicationContextConfiguration.class).sessionService();
        if(taskService == null || sessionService == null) throw new EndpointException();
        sessionService.validate(session);
        return taskService.getList(session.getUserId());
    }
    @Bean
    public List<Task> getSortedList(
            @WebParam(name = "session") final Session session,
            @WebParam(name = "method") final String method) throws Exception {
        TaskService taskService = context.getBean(ApplicationContextConfiguration.class).taskService();
        SessionService sessionService = context.getBean(ApplicationContextConfiguration.class).sessionService();
        if(taskService == null || sessionService == null) throw new EndpointException();
        sessionService.validate(session);
        return taskService.getSortedList(session.getUserId(), method);
    }
    @Bean
    public Task getOne(
            @WebParam(name = "session") final Session session,
            @WebParam(name = "taskId") final String taskId) throws Exception {
        TaskService taskService = context.getBean(ApplicationContextConfiguration.class).taskService();
        SessionService sessionService = context.getBean(ApplicationContextConfiguration.class).sessionService();
        if(taskService == null || sessionService == null) throw new EndpointException();
        sessionService.validate(session);
        return taskService.getOne(taskId, session.getUserId());
    }
    @Bean
    public void update(
            @WebParam(name = "session") final Session session,
            @WebParam(name = "task") final Task task) throws Exception {
        TaskService taskService = context.getBean(ApplicationContextConfiguration.class).taskService();
        SessionService sessionService = context.getBean(ApplicationContextConfiguration.class).sessionService();
        if(taskService == null || sessionService == null) throw new EndpointException();
        sessionService.validate(session);
        taskService.update(task);
    }
    @Bean
    public void remove(
            @WebParam(name = "session") final Session session,
            @WebParam(name = "taskId") final String taskId) throws Exception {
        TaskService taskService = context.getBean(ApplicationContextConfiguration.class).taskService();
        SessionService sessionService = context.getBean(ApplicationContextConfiguration.class).sessionService();
        if(taskService == null || sessionService == null) throw new EndpointException();
        sessionService.validate(session);
        taskService.remove(taskId, session.getUserId());
    }
    @Bean
    public void clear(
            @WebParam(name = "session") final Session session) throws Exception {
        TaskService taskService = context.getBean(ApplicationContextConfiguration.class).taskService();
        SessionService sessionService = context.getBean(ApplicationContextConfiguration.class).sessionService();
        if(taskService == null || sessionService == null) throw new EndpointException();
        sessionService.validate(session);
        taskService.clear();
    }
    @Bean
    public List<Task> find(
            @WebParam(name = "session") final Session session,
            @WebParam(name = "part") final String part) throws Exception {
        TaskService taskService = context.getBean(ApplicationContextConfiguration.class).taskService();
        SessionService sessionService = context.getBean(ApplicationContextConfiguration.class).sessionService();
        if(taskService == null || sessionService == null) throw new EndpointException();
        sessionService.validate(session);
        return taskService.find(session.getUserId(), part);
    }
}
