package ru.shumov.tm.endpoint;


import lombok.NoArgsConstructor;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Controller;
import ru.shumov.tm.application.ApplicationContextConfiguration;
import ru.shumov.tm.repository.entity.Project;
import ru.shumov.tm.repository.entity.Session;
import ru.shumov.tm.exceptions.EndpointException;
import ru.shumov.tm.service.ProjectService;
import ru.shumov.tm.service.SessionService;

import jakarta.jws.WebParam;
import jakarta.jws.WebService;
import java.util.List;

@NoArgsConstructor
@WebService
@Controller
public class ProjectEndPoint {
    ApplicationContext context = new AnnotationConfigApplicationContext(ApplicationContextConfiguration.class);

    @Bean
    public void create(
            @WebParam(name = "session") final Session session,
            @WebParam(name = "project") final Project project) throws Exception {
        ProjectService projectService = context.getBean(ApplicationContextConfiguration.class).projectService();
        SessionService sessionService = context.getBean(ApplicationContextConfiguration.class).sessionService();
        if(projectService == null || sessionService == null) throw new EndpointException();
        sessionService.validate(session);
        projectService.create(project);
    }
    @Bean
    public List<Project> getList(
            @WebParam(name = "session") final Session session) throws Exception {
        ProjectService projectService = context.getBean(ApplicationContextConfiguration.class).projectService();
        SessionService sessionService = context.getBean(ApplicationContextConfiguration.class).sessionService();
        if(projectService == null || sessionService == null) throw new EndpointException();
        sessionService.validate(session);
        return projectService.getList(session.getUserId());
    }
    @Bean
    public Project getOne(
            @WebParam(name = "session") final Session session,
            @WebParam(name = "projectId") final String projectId) throws Exception {
        ProjectService projectService = context.getBean(ApplicationContextConfiguration.class).projectService();
        SessionService sessionService = context.getBean(ApplicationContextConfiguration.class).sessionService();
        if(projectService == null || sessionService == null) throw new EndpointException();
        sessionService.validate(session);
        return projectService.getOne(projectId, session.getUserId());
    }
    @Bean
    public void update(
            @WebParam(name = "session") final Session session,
            @WebParam(name = "project") final Project project) throws Exception {
        ProjectService projectService = context.getBean(ApplicationContextConfiguration.class).projectService();
        SessionService sessionService = context.getBean(ApplicationContextConfiguration.class).sessionService();
        if(projectService == null || sessionService == null) throw new EndpointException();
        sessionService.validate(session);
        projectService.update(project);
    }

    public List<Project> getSortedList(
            @WebParam(name = "session") final Session session,
            @WebParam(name = "method") final String method) throws Exception {
        ProjectService projectService = context.getBean(ApplicationContextConfiguration.class).projectService();
        SessionService sessionService = context.getBean(ApplicationContextConfiguration.class).sessionService();
        if(projectService == null || sessionService == null) throw new EndpointException();
        sessionService.validate(session);
        return projectService.getSortedList(session.getUserId(), method);
    }
    @Bean
    public void remove(
            @WebParam(name = "session") final Session session,
            @WebParam(name = "projectId") final String projectId) throws Exception {
        ProjectService projectService = context.getBean(ApplicationContextConfiguration.class).projectService();
        SessionService sessionService = context.getBean(ApplicationContextConfiguration.class).sessionService();
        if(projectService == null || sessionService == null) throw new EndpointException();
        sessionService.validate(session);
        projectService.remove(projectId, session.getUserId());
    }
    @Bean
    public void clear(
            @WebParam(name = "session") final Session session) throws Exception {
        ProjectService projectService = context.getBean(ApplicationContextConfiguration.class).projectService();
        SessionService sessionService = context.getBean(ApplicationContextConfiguration.class).sessionService();
        if(projectService == null || sessionService == null) throw new EndpointException();
        sessionService.validate(session);
        projectService.clear();
    }
    @Bean
    public List<Project> find(
            @WebParam(name = "session") final Session session,
            @WebParam(name = "part") final String part) throws Exception {
        ProjectService projectService = context.getBean(ApplicationContextConfiguration.class).projectService();
        SessionService sessionService = context.getBean(ApplicationContextConfiguration.class).sessionService();
        if(projectService == null || sessionService == null) throw new EndpointException();
        sessionService.validate(session);
        return projectService.find(session.getUserId(), part);
    }
}
