package ru.shumov.tm.endpoint;

import lombok.NoArgsConstructor;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Controller;
import ru.shumov.tm.application.ApplicationContextConfiguration;
import ru.shumov.tm.repository.entity.Session;
import ru.shumov.tm.repository.entity.User;
import ru.shumov.tm.exceptions.EndpointException;
import ru.shumov.tm.service.SessionService;
import ru.shumov.tm.service.UserService;

import jakarta.jws.WebParam;
import jakarta.jws.WebService;



@NoArgsConstructor
@WebService
@Controller
public class UserEndPoint {
    ApplicationContext context = new AnnotationConfigApplicationContext(ApplicationContextConfiguration.class);
    @Bean
    public void create(
            @WebParam(name = "login") final String login,
            @WebParam(name = "password") final String password) throws Exception{
        UserService userService = context.getBean(ApplicationContextConfiguration.class).userService();
        SessionService sessionService = context.getBean(ApplicationContextConfiguration.class).sessionService();
        if(userService == null || sessionService == null) throw new EndpointException();
        userService.create(login, password);
    }
    @Bean
    public void passwordUpdate(
            @WebParam(name = "session") final Session session,
            @WebParam(name = "user") final User user,
            @WebParam(name = "password") final String password) throws Exception {
        UserService userService = context.getBean(ApplicationContextConfiguration.class).userService();
        SessionService sessionService = context.getBean(ApplicationContextConfiguration.class).sessionService();
        if(userService == null || sessionService == null) throw new EndpointException();
        sessionService.validate(session);
        userService.passwordUpdate(user, password);
    }
    @Bean
    public User getOne(
            @WebParam(name = "login") final String login) throws Exception {
        UserService userService = context.getBean(ApplicationContextConfiguration.class).userService();
        SessionService sessionService = context.getBean(ApplicationContextConfiguration.class).sessionService();
        if(userService == null || sessionService == null) throw new EndpointException();
        return userService.getOne(login);
    }
}
